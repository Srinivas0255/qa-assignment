package com.Qa.BaseClass;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseClass {

	public WebDriver driver = null;

	@BeforeSuite
	public void LaunchBrowser()
	{
		System.setProperty("webdriver.chrome.driver", ".//Browsers//chromedriver.exe");
		driver = new ChromeDriver();
	}

	@AfterSuite
	public void CloseBrowser()
	{
		/*	if(driver != null)
		{
			driver.quit();
		}*/
		Reporter.log("Application is closed Successfully" , true);

	}

	public void launchTheApplication()
	{
		driver.get("http://www.globalsqa.com/angularjs-protractor-practice-site/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

}
