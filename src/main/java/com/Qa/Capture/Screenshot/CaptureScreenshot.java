package com.Qa.Capture.Screenshot;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CaptureScreenshot {

	WebDriver driver = null;

	public CaptureScreenshot(WebDriver driver)
	{
		this.driver = driver;
	}

	public String takeScreenShot(String screenshotName) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String filepath = System.getProperty("user.dir")+"/Reports/Screenshots/"+screenshotName+".PNG";


		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(filepath));
		}

		catch (IOException e)
		{
			System.out.println(e.getMessage());

		}

		return filepath;
	}

}
