package com.Qa.PagesWithObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SimpleCalculatorPage {

	WebDriver driver = null;
	
	public void Click_SimpleCalculator(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,500)");
		Thread.sleep(1000);
		js.executeScript("document.getElementsByClassName('button tiny_button button_pale regular_text')[7].click();");
	}
	
	public void calculationsmethod(WebDriver driver)
	{
		WebElement a = driver.findElement(By.xpath("//tbody/tr[1]/td[2]/input"));
		WebElement b = driver.findElement(By.xpath("//tbody/tr[2]/td[2]/input"));
		a.sendKeys("5");
		b.sendKeys("6");
		Select select = new Select(driver.findElement(By.xpath("/html/body/div/table/tbody/tr[3]/td[2]/select")));
		select.selectByIndex(1);
		System.out.println("Operation doing is  " +  driver.findElement(By.xpath("//div[@class='ng-scope']/b")).getText());
	}
}
