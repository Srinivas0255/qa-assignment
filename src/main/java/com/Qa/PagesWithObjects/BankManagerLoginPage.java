package com.Qa.PagesWithObjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class BankManagerLoginPage {
	
	WebDriver driver =null;

	
	public void Click_BankManagerLogin_Button(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementsByTagName('button')[3].click();");
	}

	public void Click_Create_AddCustomer(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementsByTagName('button')[2].click();");
	}
	
	public void Fill_Customer_detials(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementsByTagName('input')[0].value='boss';");
		Thread.sleep(500);
		js.executeScript("document.getElementsByTagName('input')[1].value='bigger';");
		Thread.sleep(500);
		js.executeScript("document.getElementsByTagName('input')[2].value='cdfg';");
		Thread.sleep(500);
		js.executeScript("document.getElementsByTagName('button')[5].click();");
		Thread.sleep(500);
		Alert alert=driver.switchTo().alert();
		alert.accept();
	}
	
	public void Open_Account(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementsByTagName('button')[3].click();");
		Thread.sleep(500);
		js.executeScript("document.getElementById('userSelect').value='3';");
		Thread.sleep(500);
		js.executeScript("document.getElementById('currency').value='Dollar';");
		Thread.sleep(500);
		js.executeScript("document.getElementsByTagName('button')[5].click();");
		Thread.sleep(500);
		Alert alert=driver.switchTo().alert();
		alert.accept();
	}

}
