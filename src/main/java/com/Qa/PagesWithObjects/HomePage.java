package com.Qa.PagesWithObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class HomePage {

	WebDriver driver = null;

	public void Scroll_To_Table(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,500)");
	}

	public void ClickOn_Banking(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementsByClassName('button tiny_button button_pale regular_text')[6].click();");
	}

	public void ClickOn_CustomerLogin(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementsByTagName('button')[2].click();");
	}

	public void Select_Dropdown_value(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(1000);
		Select select = new Select(driver.findElement(By.id("userSelect")));
		select.selectByValue("3");
	}

	public void Click_login(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementsByTagName('button')[2].click();");
	}

	public void Click_AccountNumber_Dropdown(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(5000);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		Select select = new Select(driver.findElement(By.id("accountSelect")));
		select.selectByIndex(2);
		String AccountNumber = js.executeScript("return document.getElementsByClassName('ng-binding')[1].innerHTML").toString();
		String balance = js.executeScript("return document.getElementsByClassName('ng-binding')[2].innerHTML").toString();
		String Currency = js.executeScript("return document.getElementsByClassName('ng-binding')[3].innerHTML").toString();
		System.out.println("Selected Account Number is " + AccountNumber
				+ "  Amount is " + balance 
				+ "  Currency is " + Currency);
	}

	public void Click_Deposit(WebDriver driver) throws InterruptedException
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		Thread.sleep(500);
		js.executeScript("document.getElementsByClassName('btn btn-lg tab')[1].click();");
		Thread.sleep(1000);
		driver.findElement(By.tagName("input")).sendKeys("10");
		Thread.sleep(500);
		js.executeScript("document.getElementsByTagName('button')[5].click();");
		Thread.sleep(500);
		String Success =js.executeScript("return document.getElementsByTagName('span')[2].innerHTML").toString();
		System.out.println("Successfull Message "  + Success );

	}
	public void Click_WithDrawl(WebDriver driver) throws InterruptedException
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		Thread.sleep(500);
		js.executeScript("document.getElementsByClassName('btn btn-lg tab')[2].click();");
		Thread.sleep(1000);
		driver.findElement(By.tagName("input")).sendKeys("10");
		Thread.sleep(500);
		js.executeScript("document.getElementsByTagName('button')[5].click();");
		Thread.sleep(500);
		js.executeScript("document.getElementsByTagName('button')[5].click();");
		Thread.sleep(500);
		String Success =js.executeScript("return document.getElementsByTagName('span')[2].innerHTML").toString();
		System.out.println("Successfull Message "  + Success );
		Thread.sleep(500);
	}
	
	public void Click_Logout(WebDriver driver) throws InterruptedException
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementsByTagName('button')[1].click();");
		Thread.sleep(1000);
		js.executeScript("document.getElementsByTagName('button')[0].click();");
		Thread.sleep(1000);
	}

}
